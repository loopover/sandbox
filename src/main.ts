import Vue from 'vue'
import App from './App.vue'
import 'normalize.css'
import { Game } from './game';

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

declare global {
  interface Window {
    app: App
    game: Game
  }
}
