# Loopover Sandbox

This app exposes two global variables to `window`: `app` and `game`

- `app` is a `App` instance from `src/App.vue`.
- `game` is an instance of `Game`, located in `src/game/Game.ts`

## Useful methods

- `app.executeMoves(moveNotation)`
- `game.scrable()`
- `game.animatedMove(axis, index, n)`
- `game.setBoardSize(cols, rows)`
- `game.board.pos(index)`
- `game.board.toJSON()`